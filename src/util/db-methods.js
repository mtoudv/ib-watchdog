let mysql = require("mysql")

createConn = function() {
return mysql.createConnection({
    host     : 'productiondb.cqa1nh3xckun.eu-central-1.rds.amazonaws.com',
    user     : 'watchdog_dan',
    password : 'bhaG3gb5:AWNm\\6T',
    database : 'watchdog'
    });
}

query = function (sql, callback, conn, end) {
  conn.query(sql, function (err, rows) {
    if (err) {
      conn.rollback(() => {
        errorLog(err, sql, callback)
      })
    } else if (end === true) {
      endTransaction(conn, callback, rows)
    } else {
      callback(err, rows, conn)
    }
  })
}

transaction = function (sql, callback, conn = undefined, endTransaction = true) {
  if(conn === undefined) {
    conn = createConn()
    conn.connect((err) => { if(err) errorLog(err, sql, callback) })
    /* Begin transaction */
    conn.beginTransaction(function(err) {
      if (err) {
        errorLog(err, sql, callback)
      } else {
        // if no error on making transaction, we do what we need and wee send and error counter callback for the transaction to handle
        query(sql, callback, conn, endTransaction)
      }
    })
  } else {
    query(sql, callback, conn, endTransaction)
  }
  /* End transaction */
}

endTransaction = function (conn, callback = () => {}, rows = undefined) {
  conn.commit((err) => {
    if (err) {
      conn.rollback(() => {
        errorLog(err, sql, callback)
      })
    } else {
      conn.end(function () {
        callback(err, rows)
      })
    }
  })
}

errorLog = function (err, sql, callback) {
    console.log("Transaction Error: ", err, sql)
    callback(err)
}

module.exports.createConn = createConn;
module.exports.query = query;
module.exports.transaction = transaction;
module.exports.endTransaction = endTransaction;