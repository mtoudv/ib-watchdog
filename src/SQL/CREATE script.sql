CREATE TABLE alarms (
    alarm_id MEDIUMINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(64),
    description VARCHAR(1024),
    PRIMARY KEY (alarm_id)
);

CREATE TABLE checks (
    check_id MEDIUMINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(64),
    description VARCHAR(1024),
    PRIMARY KEY (check_id)  
);

CREATE TABLE actions (
    action_id MEDIUMINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(64),
    description VARCHAR(1024),
    PRIMARY KEY (action_id)      
);

CREATE TABLE alarm_checks_relations (
    alarm_id MEDIUMINT NOT NULL,
    check_id MEDIUMINT NOT NULL,
    FOREIGN KEY (alarm_id) REFERENCES alarms(alarm_id),
    FOREIGN KEY (check_id) REFERENCES checks(check_id)
);

CREATE TABLE alarm_actions_relations (
    alarm_id MEDIUMINT NOT NULL,
    action_id MEDIUMINT NOT NULL,
    FOREIGN KEY (alarm_id) REFERENCES alarms(alarm_id),
    FOREIGN KEY (action_id) REFERENCES actions(action_id)
);

CREATE TABLE actions_hipchat (
    hipchat_id int PRIMARY KEY REFERENCES actions(action_id),
    room_id varchar(64) NOT NULL,
    message varchar(1024) NOT NULL,
    message_format varchar(64) NOT NULL,
    color varchar(64) NOT NULL
);

CREATE TABLE checks_pagespeedinsights (
    psi_id int PRIMARY KEY REFERENCES checks(check_id),
    desktop_threshold TINYINT NOT NULL,
    laptop_theshold TINYINT NOT NULL,
    url VARCHAR(64) NOT NULL,
    api_key VARCHAR(64)) NOT NULL,
    filter_third_party_resources BOOLEAN,
    locale VARCHAR(64),
    screenshot BOOLEAN,
    strategy VARCHAR(64) NOT NULL,
    score_threshold tinyint NOT NULL
);

CREATE TABLE results_pagespeedinsights (
    pagespeedinsights_result_id MEDIUMINT NOT NULL AUTO_INCREMENT,
    checked TIMESTAMP(),
    score TINYINT NOT NULL,
    insights TEXT NOT NULL,
    FOREIGN KEY (psi_result_id) REFERENCES checks_pagespeedinsights(psi_id)
);