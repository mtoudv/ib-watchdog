var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

var Hipchatter = require('hipchatter');
var hipchatter = new Hipchatter(process.env.HIPCHAT_API_TOKEN);
const HIPCHAT = 'HipChat';

const CALLBACK_MESSAGE = 'Successfully evaluated all alarms!';
var alarms;
var evaluatedAlarms = 0;

exports.handler = function(event, context, callback) {
    fetchAndEvaluateAlarms();

    callback(null, CALLBACK_MESSAGE);
}

function fetchAndEvaluateAlarms() {
    let query = 'SELECT alarm_id, isActive'
    + ' FROM alarms';

    db.transaction(query, function (err, result, conn) {
        if (err) throw err;
        
        alarms = result;
        console.log('Successfully fetched ' + alarms.length + ' alarms');

        for (var i = 0; i < alarms.length; i++) {
            evaluateAlarm(alarms[i], conn);            
        }
    }, undefined, false);
}

function evaluateAlarm(alarm, conn) {
    let query = 'SELECT c.isTriggered'
    + ' FROM alarm_checks_relations acr'
    + ' INNER JOIN checks c ON c.check_id = acr.check_id' 
    + ' WHERE acr.alarm_id = ' + alarm.alarm_id;

    db.transaction(query, function (err, result, conn) {
        evaluatedAlarms++;
        var checks = result;
        var amountOfChecks = checks.length;
        var amountIsTriggered = 0;

        for (var i = 0; i < checks.length; i++) {
            if (checks[i].isTriggered === 1) {
                amountIsTriggered++;
            }
        }

        //Invoke alarm if the alarm contains checks, the alarm is active and all its checks are triggered
        if (amountOfChecks !== 0 && alarm.isActive === 1 && amountOfChecks === amountIsTriggered) {
            invokeAlarm(alarm, conn);
            console.log('Invoking Alarm with id: ' + alarm.alarm_id);
        }
        
        if(evaluatedAlarms === alarms.length) {  
            db.endTransaction(conn);
            console.log('All alarms evaluated - Closing DB Connection!');   
        }
    }, conn, false);
}

function invokeAlarm(alarm, conn) {
    let query = 'SELECT *'
    + ' FROM alarm_actions_relations acr'
    + ' INNER JOIN actions a ON a.action_id = acr.action_id' 
    + ' WHERE acr.alarm_id = ' + alarm.alarm_id;

    db.transaction(query, function (err, result, conn) {
        if (err) throw err;
        
        var actions = result; 

        for (var i = 0; i < actions.length; i++) {
            evaluateActionTarget(actions[i]);
        }
    }, conn, false);
}

/*
For scalability, the evaluateActionTarget function might be an appropiate place to delegate to other lambda functions
*/

function evaluateActionTarget(action) {
    console.log('action target: ' + action.target);

    if (action.target === HIPCHAT) {
        invokeHipchatAction(action);        
    } else if (action.target === OTHER_API) {
        console.log('TODO');
    }
}

function invokeHipchatAction(action) {
    let query = 'SELECT *'
    + ' FROM actions_hipchat'
    + ' WHERE hipchat_id = ' + action.action_id;

    db.transaction(query, function (err, result) {
        if (err) throw err;
        
        var hipchatAction = result[0];
        
        hipchatter.notify(hipchatAction.room_id, 
        {
            message: hipchatAction.message,
            message_format: hipchatAction.message_format,
            color: hipchatAction.color,
            token: process.env.HIPCHAT_ROOM_TOKEN,
            notify: hipchatAction.notify,
        }, function(err){
            if (err === null) console.log('Successfully notified room with id: ' + hipchatAction.hipchat_id);
        });
    });
}