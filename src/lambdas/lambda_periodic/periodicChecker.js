var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

const PSI = 'PagespeedInsights';
const CALLBACK_MESSAGE = 'Successfully performed periodic checks!';

exports.handler = function(event, context, callback) {
    fetchAndPerformChecks();

    callback(null, CALLBACK_MESSAGE);    
}

function fetchAndPerformChecks() {
    let query = 'SELECT check_id, target' 
    + ' FROM checks';

    db.transaction(query, function (err, result, conn) {
        if (err) throw err;

        var checks = result;

        for (var i = 0; i < checks.length; i++) {
            evaluateCheckTarget(checks[i], conn);
        }
        db.endTransaction(conn);
    }, undefined, false);
}

/*
For scalability, the evaluateCheckTarget function might be an appropiate place to delegate to other lambda functions
*/

function evaluateCheckTarget(check, conn) { 
    console.log('check target: ' + check.target);
    
    if (check.target === PSI) {
        fetchPsiCheck(check, conn);
    } else if (check.target === OTHER_API) {
        console.log('TODO');
    }
}

function fetchPsiCheck(check, conn) {
    let query = 'SELECT *' 
    + ' FROM checks_pagespeedinsights'
    + ' WHERE psi_id = ' + check.check_id;

    db.transaction(query, function (err, result, conn) {
        if (err) throw err;

        var psiCheck = result[0];

        performPsiCheck(psiCheck);
    }, conn, false);
}

function performPsiCheck(psiCheck) {
    var https = require('https');
    let psiRequest = 'https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=' + encodeURIComponent(psiCheck.url) + 
    '&key=' + psiCheck.api_key + '&strategy=' + psiCheck.strategy;

    https.get(psiRequest, function(res) {
        var body = '';

        res.on('data', function(chunk){
            body += chunk;
        });
        
        res.on('end', function(){
            var response = JSON.parse(body);
            storePsiCheckResults(response, psiCheck.psi_id);
        });
    }).on('error', function(e) {
        console.error("error?");
    });
}

function storePsiCheckResults(data, id) {
    var dateFormat = require('dateformat');

    let query = "INSERT INTO results_pagespeedinsights "
    + "VALUES ('" + "', '" + id + "', '" + dateFormat("yyyy-mm-dd HH:MM:ss") + "', '" + data.score + "', '" + JSON.stringify(data.formattedResults) + "');";

    db.transaction(query, function (err, result) {
        if (err) throw err;
        console.log('Successfully stored pagespeedinsights results in watchdog db for check with id: ' + id);
    });
}