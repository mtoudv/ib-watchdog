var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

const PSI = 'PagespeedInsights';
const CALLBACK_MESSAGE = 'Successfully updated all checks!';

exports.handler = function(event, context, callback) {
    fetchAndUpdateChecks();

    callback(null, CALLBACK_MESSAGE);
}

function fetchAndUpdateChecks() {
    let query = 'SELECT check_id, triggerCount, target'
    + ' FROM checks';

    db.transaction(query, function (err, result, conn) {
        if (err) throw err;

        var checks = result;
    
        for (var i = 0; i < checks.length; i++) {
            evaluateCheckTarget(checks[i], conn);
        }
        db.endTransaction(conn);
    }, undefined, false);
}

/*
For scalability, the evaluateCheckTarget function might be an appropiate place to delegate to other lambda functions
*/

function evaluateCheckTarget(check, conn) {
    if (check.target === PSI) {
        evaluatePsiCheck(check, conn);
    } else {
        console.log("PeriodicUpdater could not evaluate check with id: " + check.check_id);
    }
}

function evaluatePsiCheck(check, conn) {
    let query = 'SELECT c.check_id, psic.score_threshold, psir.score, psir.checked'
    + ' FROM checks c'
    + ' LEFT JOIN checks_pagespeedinsights psic ON psic.psi_id = c.check_id'
    + ' LEFT JOIN  results_pagespeedinsights psir ON psir.psi_id = psic.psi_id'
    + ' WHERE c.check_id = ' + check.check_id
    + ' ORDER BY psir.checked DESC LIMIT ' + check.triggerCount;

    db.transaction(query, function (err, result, conn) {
        if (err) throw err;

        var psiCheckResults = result;
        var checksAboveThresholdAmount = 0;

        if (!psiCheckResults[0].checked) {
            console.log('No Results found for check with id: ' + psiCheckResults[0].check_id);
            return;
        }

        for (var i = 0; i < psiCheckResults.length; i++) {
            console.log('PSI score: ' + psiCheckResults[i].score + ' of Check with id: ' + psiCheckResults[i].check_id);
            if (psiCheckResults[i].score <= psiCheckResults[0].score_threshold) {
                checksAboveThresholdAmount++;                
            }
        }

        if (checksAboveThresholdAmount === psiCheckResults.length) {
            updateIsTriggered(psiCheckResults[0].check_id, 1, conn);
        } else if (!psiCheckResults.check_id) {
            updateIsTriggered(psiCheckResults[0].check_id, 0, conn);
        }
    }, conn, false);
}

function updateIsTriggered(check_id, isTriggered, conn) {
    let query = 'UPDATE checks'
    + ' SET isTriggered = ' + isTriggered
    + ' WHERE check_id = ' + check_id;

    db.transaction(query, function (err, result) {
        if (err) throw err;
        console.log('Successfully updated isTriggered to ' + isTriggered + ' for check with id: ' + check_id);
    }, conn, false);
}
