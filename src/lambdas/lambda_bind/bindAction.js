
var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

var CALLBACK_MESSAGE = 'Successfully bound an Action to the alarm';

exports.handler = function(event, context, callback) {
    bindAction(event);

    callback(null, CALLBACK_MESSAGE);    
}

function bindAction(event) {
    let query = "INSERT INTO alarm_actions_relations"
    + " VALUES ('" + event.alarm_id + "', '" + event.action_id + "');";

    db.transaction(query, function (err, result) {
        if (err) throw err;

        console.log('Successfully bound Action with id: ' + event.action_id + ' to Alarm with id: ' + event.alarm_id);        
    }, undefined);
}
