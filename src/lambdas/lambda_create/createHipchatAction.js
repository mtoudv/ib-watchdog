var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

var resultId;
const CALLBACK_MESSAGE = 'Successfully created new Action';

exports.handler = function(event, context, callback) {
    insertAction(event);
    
    callback(null, CALLBACK_MESSAGE);
}

function insertAction(event) {
    let query = "INSERT INTO actions"
    + " VALUES ('" + event.id + "', '" + event.target + "');";
    
    db.transaction(query, function (err, result, conn) {
        if (err) throw err;

        resultId = result.insertId;

        insertHipchatAction(event, conn)
    }, undefined, false);
}

function insertHipchatAction(event, conn) {
    let query = "INSERT INTO actions_hipchat"
    + " VALUES ('" + resultId + "', '" + event.room_id + "', '" + event.message + "', '" + event.message_format
    + "', '" + event.color + "', '" + event.notify + "');";
    
    db.transaction(query, function (err, result) {
        if (err) throw err;

        console.log("Successfully inserted new hipchat action with id: " + resultId);
    }, conn);
}