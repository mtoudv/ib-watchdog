var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : DB_HOST,
  user     : DB_USER,
  password : DB_PASS,
  database : DB_NAME
});

const CALLBACK_MESSAGE = 'Successfully created new Check';

exports.handler = function(event, context, callback) {
    connectToDatabase();
    insertIntoDatabase(event);
    connection.end();
    
    callback(null, CALLBACK_MESSAGE);
}

function connectToDatabase() {
    connection.connect(function(err) {
        if (err) throw err;
        console.log('Connection to DB established!');
      });
}

function insertIntoDatabase(event) {
    let query = "INSERT INTO checks "
    + "VALUES ('" + event.id + "', '" + event.alarm_id + "', '" + event.name + "', '" + event.description + "', '" + event.isTriggered + "');";

    connection.query(query, function (err, result) {
        if (err) throw err;
        console.log('Successfully inserted new check');
    });
}