var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

const CALLBACK_MESSAGE = 'Successfully created new Alarm';

exports.handler = function(event, context, callback) {
    createAlarm(event);
    
    callback(null, CALLBACK_MESSAGE);    
}

function createAlarm(event) {
    let query = "INSERT INTO alarms "
    + "VALUES ('" + event.id + "', '" + event.name + "', '" + event.description + "', '" + event.isActive + "');";
    
    db.transaction(query, function (err, result) {
        if (err) throw err;

        console.log('Successfully inserted new alarm');
    }, undefined);
}