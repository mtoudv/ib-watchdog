var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

var resultId;
const CALLBACK_MESSAGE = 'Successfully created new PagespeedInsights Check';

exports.handler = function(event, context, callback) {
    createCheck(event);
    
    callback(null, CALLBACK_MESSAGE);
}

function createCheck(event) {
    let query = "INSERT INTO checks"
    + " VALUES ('" + event.id + "', '" + event.trigger_count + "', '" + 0 + "', '" + event.target + "');";
    
    db.transaction(query, function (err, result, conn) {
        if (err) throw err;

        resultId = result.insertId;

        createPsiCheck(event, conn)
    }, undefined, false);
}

function createPsiCheck(event, conn) {
    let query = "INSERT INTO checks_pagespeedinsights"
    + " VALUES ('" + resultId + "', '" + event.url + "', '" + event.api_key + "', '" + event.filter_third_party_resources
    + "', '" + event.locale + "', '" + event.screenshot + "', '" + event.strategy + "', '" + event.score_threshold + "');";
    
    db.transaction(query, function (err, result) {
        if (err) throw err;
        console.log("Successfully inserted new PagespeedInsights Check with id: " + resultId);
    }, conn);
}