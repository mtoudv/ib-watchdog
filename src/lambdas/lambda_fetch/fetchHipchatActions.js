var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

const CALLBACK_MESSAGE = 'Successfully fetched all HipChat Actions!';

exports.handler = function(event, context, callback) {
    fetchChecks();

    callback(null, CALLBACK_MESSAGE);         
}

function fetchChecks() {
    let query = 'SELECT a.*, hc.*'
    + ' FROM actions a'
    + ' LEFT JOIN actions_hipchat hc'
    + ' ON a.action_id = hc.hipchat_id';

    db.transaction(query, function (err, result) {
        if (err) throw err;

        console.log("Successfully fetched " + result.length + " PagespeedInsights Checks from the Watchdog DB!");
    }, undefined);
}