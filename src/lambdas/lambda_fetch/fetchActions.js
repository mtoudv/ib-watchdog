var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

const CALLBACK_MESSAGE = 'Successfully fetched all actions!';

exports.handler = function(event, context, callback) {
    fetchActions();

    callback(null, CALLBACK_MESSAGE);   
}

function fetchActions() {
    let query = 'SELECT *'
    + ' FROM actions';
    
    db.transaction(query, function (err, result) {
        if (err) throw err;

        console.log("Successfully fetched " + result.length + " actions from the Watchdog DB!");
    }, undefined);
}