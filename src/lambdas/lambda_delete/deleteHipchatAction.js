var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

const CALLBACK_MESSAGE = 'Successfully deleted the Action';

exports.handler = function(event, context, callback) {
    deleteAction(event);

    callback(null, CALLBACK_MESSAGE);    
}

function deleteAction(event) {
    let query = "DELETE FROM actions"
    + " WHERE action_id=" + event.action_id + ";";

    transaction(query, function (err, result, conn) {
        if (err) throw err;

        deleteHipchatAction(event, conn)
    }, undefined, false);
}

function deleteHipchatAction(event, conn) {
    let query = "DELETE FROM actions_hipchat"
    + " WHERE hipchat_id=" + event.action_id + ";";
    
    transaction(query, function (err, result) {
        if (err) throw err;
        console.log('Deleted Hipchat Action with id: ' + event.action_id);        
    }, conn);
}