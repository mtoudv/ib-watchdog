var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

const CALLBACK_MESSAGE = 'Successfully deleted the Check';

exports.handler = function(event, context, callback) {
    deleteCheck(event);

    callback(null, CALLBACK_MESSAGE);    
}

function deleteCheck(event) {
    let query = "DELETE FROM checks"
    + " WHERE check_id=" + event.check_id + ";";

    db.transaction(query, function (err, result, conn) {
        if (err) throw err;

        deletePsiCheck(event, conn)
    }, undefined, false);
}

function deletePsiCheck(event, conn) {
    let query = "DELETE FROM checks_pagespeedinsights"
    + " WHERE psi_id=" + event.check_id + ";";
    
    db.transaction(query, function (err, result) {
        if (err) throw err;

        console.log('Deleted PagespeedInsights Check with id: ' + event.check_id);
    }, conn);
}