var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

const CALLBACK_MESSAGE = 'Successfully deleted the Alarm';

exports.handler = function(event, context, callback) {
    deleteAlarm(event);

    callback(null, CALLBACK_MESSAGE);    
}

function deleteAlarm(event) {
    let query = "DELETE FROM alarms "
    + "WHERE alarm_id=" + event.alarm_id + ";";
    
    transaction(query, function (err, result, conn) {
        if (err) throw err;

        console.log('Successfully deleted Alarm with id: ' + event.alarm_id);
    }, undefined);
}