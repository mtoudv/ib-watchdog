
var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

const CALLBACK_MESSAGE = 'Successfully bound Check to Alarm!';

exports.handler = function(event, context, callback) {
    unbindCheck(event);

    callback(null, CALLBACK_MESSAGE);    
}

function unbindCheck(event) {
    let query = "DELETE FROM alarm_checks_relations"
    + " WHERE check_id=" + event.check_id + ";";

    db.transaction(query, function (err, result) {
        if (err) throw err;

        console.log('Successfully unbound check from the alarm');
    }, undefined);
}
