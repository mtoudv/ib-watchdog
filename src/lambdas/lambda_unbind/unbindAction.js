
var db      = require('./util/db-methods');
var DB_HOST = process.env.DB_HOST;
var DB_NAME = process.env.DB_NAME;
var DB_USER = process.env.DB_USER;
var DB_PASS = process.env.DB_PASS;

const CALLBACK_MESSAGE = 'Successfully bound Action to Alarm!';

exports.handler = function(event, context, callback) {
    unbindAction(event);  

    callback(null, CALLBACK_MESSAGE);    
}

function unbindAction(event) {
    let query = "DELETE FROM alarm_actions_relations"
    + " WHERE action_id=" + event.action_id + ";";

    db.transaction(query, function (err, result) {
        if (err) throw err;

        console.log('Successfully unbound action from the alarm');
    }, undefined);
}
