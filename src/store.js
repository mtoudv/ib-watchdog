
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    checkTypes: [
     { psi: true },
     { database: false },
    ],
    actionTypes: [
      { hipchat: false },
      { database: false },
    ],
    createAlarmToggled: false,
    createCheckToggled: false,
    createActionToggled: false,
    psiType: true,
  },
  getters: {
    createAlarmToggled: state => state.createAlarmToggled,
    createCheckToggled: state => state.createCheckToggled,
    createActionToggled: state => state.createActionToggled,
    psiType: state => state.psiType,
  },
  mutations: {
    toggleCreateAlarm(state) {
      if (state.createAlarmToggled) {
        this.state.createAlarmToggled = false;
      } else if (!state.createAlarmToggled) {
        this.state.createAlarmToggled = true;
      }
    },
    toggleCreateCheck(state) {
      if (state.createCheckToggled) {
        this.state.createCheckToggled = false;
      } else if (!state.createCheckToggled) {
        this.state.createCheckToggled = true;
      }
    },
    toggleCreateAction(state) {
      if (state.createActionToggled) {
        this.state.createActionToggled = false;
      } else if (!state.createActionToggled) {
        this.state.createActionToggled = true;
      }
    },
    /*selectPsiCheck() {
      this.state.checkTypes.forEach(function(type) {
        Object.keys(type).forEach(function(key) {
          if (type === 'psi') {
            type[key] = false;
          } else {
            type[key] = false;
          }
        });
      });
    },*/
  },
  actions: {
    toggleCreateAlarm(context) {
      context.commit('toggleCreateAlarm');
    },
    toggleCreateCheck(context) {
      context.commit('toggleCreateCheck');
    },
    toggleCreateAction(context) {
      context.commit('toggleCreateAction');
    },
    selectPsiCheck(context) {
      context.commit('selectPsiCheck');
    },
  },
});
