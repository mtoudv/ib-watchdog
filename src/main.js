// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import store from './store';
import ListerChecks from './components/ListerChecks';
import ListerActions from './components/ListerActions';
import ListerAlarms from './components/ListerAlarms';
import Lister from './components/Lister';
import Creator from './components/Creator';
import Watchdog from './components/Watchdog';
import CreateAlarm from './components/CreateAlarm';
import CreateCheck from './components/CreateCheck';
import CreatePsiCheck from './components/CreatePsiCheck';
import CreateAction from './components/CreateAction';
import CreateHipchatAction from './components/CreateHipchatAction';


Vue.config.productionTip = false;

Vue.component('watchdog', Watchdog);
Vue.component('lister', Lister);
Vue.component('creator', Creator);
Vue.component('lister-alarms', ListerAlarms);
Vue.component('lister-checks', ListerChecks);
Vue.component('lister-actions', ListerActions);
Vue.component('create-alarm', CreateAlarm);
Vue.component('create-check', CreateCheck);
Vue.component('create-psi-check', CreatePsiCheck);
Vue.component('create-action', CreateAction);
Vue.component('create-hipchat-action', CreateHipchatAction);


/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  template: '<App/>',
  components: { App },
});
/*
data: {
    alarms: [],
    checks: [],
    actions: [],
  },
  methods: {
    fetchAlarms() {
      axios.get(alarmsApi).then((response) => {
        this.alarms = JSON.parse(response.data);
      }, (error) => {
        console.log(error);
      });
    },
    fetchChecks() {
      axios.get(checksApi).then((response) => {
        this.checks = JSON.parse(response.data);
      }, (error) => {
        console.log(error);
      });
    },
    fetchActions() {
      axios.get(actionsApi).then((response) => {
        this.actions = JSON.parse(response.data);
      }, (error) => {
        console.log(error);
      });
    },
  },
  created() {
    this.fetchActions();
    this.fetchAlarms();
    this.fetchChecks();
  },
  */
