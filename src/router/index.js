import Vue from 'vue';
import Router from 'vue-router';
import Watchdog from '@/components/Watchdog';
import Creator from '@/components/Creator';
import Lister from '@/components/Lister';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Watchdog',
      component: Watchdog,
    },
    {
      path: '/creator',
      name: 'Creator',
      component: Creator,
    },
    {
      path: '/lister',
      name: 'Lister',
      component: Lister,
    },
  ],
});
